package cn.demoframe.test.frame.controller;

import cn.demoframe.test.frame.model.MenuInfo;
import cn.demoframe.test.frame.service.MenuinfoService;
import cn.demoframe.test.model.Griddata;
import cn.demoframe.test.model.Griddata_pageinfo;
import cn.demoframe.test.model.Message;
import cn.demoframe.test.utils.DateToolkit;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class MenuController {
    private static final Logger logger = LoggerFactory.getLogger(Thread
            .currentThread().getStackTrace()[1].getClassName());

    @Resource(name = "MenuinfoServiceImpl")
    private MenuinfoService menuinfoService;

    @RequestMapping(value = "/auth/menuinfo/getmenuinfos", method = RequestMethod.POST)
    @ResponseBody
    public Griddata_pageinfo getMenuinfos() {
        logger.debug("获取菜单信息");
        List<MenuInfo> menulist = menuinfoService.getMenuInfos(null,
                null, null, "menuid ");
        return new Griddata_pageinfo(menulist.size(), menulist);
    }

    /**
     * 修改菜单
     * @return 信息
     */
    @RequestMapping(value = { "/auth/menuinfo/modmenu" }, method = { RequestMethod.POST })
    @ResponseBody
    public Message modMenu_log(HttpServletRequest request,
    		@RequestParam(value = "menuid") Integer menuid,
    		@RequestParam(value = "title") String title,
    		@RequestParam(value = "nodetype") String nodetype,
    		@RequestParam(value = "position") Integer position,
    		@RequestParam(value = "parentid") Integer parentid,
    		@RequestParam(value = "url") String url) {
    	Message m = new Message();
        try {
        	menuinfoService.modMenuInfo(menuid, title, nodetype, position, parentid, url);
			m.setResult("00");
			m.setDetail("角色权限已修改.");
		} catch (Exception e) {
			m.setResult("99");
			m.setDetail("角色权限修改失败.");
			logger.error("角色权限修改失败.", e);
		}
        return m;
    }
    
    @SuppressWarnings("unchecked")
	@RequestMapping(value = { "/auth/menuinfo/modmenutree" }, method = { RequestMethod.POST })
    @ResponseBody
    public Message modMenuTree_log(HttpServletRequest request, @RequestParam(value = "data") String data){
    	Message m = new Message();
    	try {
			JSONArray jsArr = JSONArray.parseArray(data);
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			Map<String, Object> map;
			for(int i = 0; i < jsArr.size(); i++){
				JSONObject json = jsArr.getJSONObject(i);
				map = (Map<String, Object>) JSONObject.toJavaObject(json, Map.class);
				list.add(map);
			}
			menuinfoService.modMenuInfos(list);
			m.setResult("00");
			m.setDetail("修改菜单成功.");
		} catch (Exception e) {
			m.setResult("99");
			m.setDetail("修改菜单失败.");
			logger.error("修改菜单失败.", e);
		}
    	return m;
    }

	/**
	 * 新增菜单
	 * @param menuid 菜单ID
	 * @param title 标题
	 * @param nodetype 节点类型
	 * @param position 页面位置
	 * @param parentid 父节点ID
	 * @param url 地址
	 * @return 信息
	 */
    @RequestMapping(value = { "/auth/menuinfo/addmenu" }, method = { RequestMethod.POST })
    @ResponseBody
    public Message addMenu_log(HttpServletRequest request,
    		@RequestParam(value = "menuid") Integer menuid,
    		@RequestParam(value = "title") String title,
    		@RequestParam(value = "nodetype") String nodetype,
    		@RequestParam(value = "position") Integer position,
    		@RequestParam(value = "parentid") Integer parentid,
    		@RequestParam(value = "url") String url) {
        Message m = new Message();
        if(menuinfoService.getMenuInfosCount(menuid, null, null) > 0){
        	 m.setResult("99");
             m.setDetail("菜单号已存在.");
             return m;
        }
        try {
			MenuInfo menu = new MenuInfo();
			menu.setMenuid(menuid);
			menu.setTitle(title);
			menu.setNodetype(nodetype);
			menu.setPosition(position);
			menu.setParentid(parentid);
			menu.setUrl(url);
			menu.setCreatetime(DateToolkit.Date2String(new Date(), DateToolkit.YYYY_MM_DD_HH24_MM_SS));
			menuinfoService.addMenuInfo(menu);
			m.setResult("00");
			m.setDetail("菜单新增成功.");
		} catch (Exception e) {
			m.setResult("99");
			m.setDetail("菜单新增失败.");
			logger.error("菜单新增失败.", e);
		}

        return m;
    }

    /**
     * 删除菜单
     * @param menuid 菜单ID
     * @return 信息
     */
    @RequestMapping(value = { "/auth/menuinfo/delmenu" }, method = { RequestMethod.POST })
    @ResponseBody
    public Message delMenu_log(HttpServletRequest request,
            @RequestParam(value = "menuid") Integer menuid) {
        Message m = new Message();
        try {
			menuinfoService.delMenuInfo(menuid);
			m.setResult("00");
			m.setDetail("菜单已删除.");
		} catch (Exception e) {
			m.setResult("99");
			m.setDetail("菜单删除失败.");
			logger.error("菜单删除失败.", e);
		}
        return m;
    }

}
