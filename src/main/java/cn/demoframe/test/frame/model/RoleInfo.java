package cn.demoframe.test.frame.model;

public class RoleInfo {
    private String roleid;
    private String rolenm;
    private String lmtserial;
    private String createtime;

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getRolenm() {
        return rolenm;
    }

    public void setRolenm(String rolenm) {
        this.rolenm = rolenm;
    }

    public String getLmtserial() {
        return lmtserial;
    }

    public void setLmtserial(String lmtserial) {
        this.lmtserial = lmtserial;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }
    @Override
    public String toString() {
        return "roleinfo [lmtserial=" + lmtserial + ", roleid=" + roleid
                + ", rolenm=" + rolenm + "]";
    }

}
