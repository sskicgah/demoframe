package cn.demoframe.test.frame.model;


public class UserInfo {
    private String userid; // 用户标识
    private String parentid; // 母账户ID
    private String usernm; // 用户名称
    private String passwd; // 密码
    private String roleid; // 角色标识
    private String rolenm; // 角色名称
    private String email; // 电子邮件
    private String mobile; // 手机号码
    private String remark; // 备注
    private String lmtserial; // 权限字符串
	private String createtime; // 创建时间
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public String getUsernm() {
		return usernm;
	}
	public void setUsernm(String usernm) {
		this.usernm = usernm;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getRoleid() {
		return roleid;
	}
	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}
	public String getRolenm() {
		return rolenm;
	}
	public void setRolenm(String rolenm) {
		this.rolenm = rolenm;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getLmtserial() {
		return lmtserial;
	}
	public void setLmtserial(String lmtserial) {
		this.lmtserial = lmtserial;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

}
