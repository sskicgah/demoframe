package cn.demoframe.test.frame.vmodel;

public class RoleForm {
    private String roleid; // 角色标识
    private String rolenm; // 角色名称

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getRolenm() {
        return rolenm;
    }

    public void setRolenm(String rolenm) {
        this.rolenm = rolenm;
    }

}
