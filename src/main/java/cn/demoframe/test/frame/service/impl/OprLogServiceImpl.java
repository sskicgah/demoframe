package cn.demoframe.test.frame.service.impl;

import cn.demoframe.test.core.dao.CommonDao;
import cn.demoframe.test.frame.model.OprLog;
import cn.demoframe.test.frame.service.OprLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("OprLogServiceImpl")
public class OprLogServiceImpl implements OprLogService {

    @Resource
    private CommonDao commonDao;

    public void addOprLog(OprLog instance) {
     	commonDao.insertInfo("insertOprLog", instance);
    }

    public List<OprLog> getOprLogs(String opuserid, String optype, String sortorder) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("opuserId", opuserid);
        index.put("opType", optype);
        index.put("sortorder", sortorder);

        return commonDao.selectInfo("selectOprLogs", index);
    }

    public int getOprLogsCount(String opuserid, String optype) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("opuserId", opuserid);
        index.put("opType", optype);
        return commonDao.selectInfoCount("selectOprLogsCount", index);
    }
}
