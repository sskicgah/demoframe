package cn.demoframe.test.frame.service.impl;

import cn.demoframe.test.core.dao.CommonDao;
import cn.demoframe.test.frame.model.MenuInfo;
import cn.demoframe.test.frame.service.MenuinfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("MenuinfoServiceImpl")
public class MenuinfoServiceImpl implements MenuinfoService {

    @Resource
    private CommonDao commonDao;

    public void addMenuInfo(MenuInfo instance) {
     	commonDao.insertInfo("insertMenuInfo", instance);
    }

    public void delMenuInfo(Integer menuid) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("menuid", menuid);
        commonDao.deleteInfo("deleteMenuInfo", index);
    }

    public List<MenuInfo> getMenuInfos(Integer menuid, String nodetype, Integer parentid,
            String sortorder) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("menuid", menuid);
        index.put("nodetype", nodetype);
        index.put("parentid", parentid);
        index.put("sortorder", sortorder);

        return commonDao.selectInfo("selectMenuInfos", index);
    }

    public int getMenuInfosCount(Integer menuid, String nodetype, Integer parentid) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("menuid", menuid);
        index.put("nodetype", nodetype);
        index.put("parentid", parentid);
        return commonDao.selectInfoCount("selectMenuInfosCount", index);
    }

    public void modMenuInfo(Integer menuid, String title, String nodetype, Integer position,
            Integer parentid, String url) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("menuid", menuid);
        index.put("title", title);
        index.put("nodetype", nodetype);
        index.put("position", position);
        index.put("parentid", parentid);
        index.put("url", url);
        commonDao.updateInfo("updateMenuInfo", index);
    }

    public void modMenuInfos(List<Map<String, Object>> indexs){
    	commonDao.updateInfos("updateMenuInfo", indexs);
    }
}
