package cn.demoframe.test.frame.service;

import java.util.List;
import java.util.Map;

import cn.demoframe.test.frame.model.MenuInfo;

public interface MenuinfoService {

    List<MenuInfo> getMenuInfos(Integer menuid, String nodetype, Integer parentid,
								String sortorder);

    int getMenuInfosCount(Integer menuid, String nodetype, Integer parentid);

    void addMenuInfo(MenuInfo instance);

    void modMenuInfo(Integer menuid, String title, String nodetype, Integer position,
					 Integer parentid, String url);
    
    void modMenuInfos(List<Map<String, Object>> maps);

    void delMenuInfo(Integer menuid);

}
