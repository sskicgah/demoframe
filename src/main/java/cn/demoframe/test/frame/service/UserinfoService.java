package cn.demoframe.test.frame.service;

import java.util.List;

import cn.demoframe.test.frame.model.UserInfo;

public interface UserinfoService {

    List<UserInfo> getUserInfos(String parentid, String userid, String usernm, String passwd,
								String roleid, Integer status, Integer startRow, Integer pageSize, String sortorder);

    int getUserInfosCount(String userid, String usernm, String passwd, String roleid,
						  Integer status);

    void addUserInfo(UserInfo instance);
    
    void addUserInfos(List<UserInfo> instance);

    void modUserInfo(String userid, String usernm, String passwd,
					 String roleid, String email, String mobile, String remark, Integer status);

    void delUserInfo(String userid);

}
