package cn.demoframe.test.frame.service;

import java.util.List;

import cn.demoframe.test.frame.model.RoleInfo;

public interface RoleinfoService {

    List<RoleInfo> getRoleInfos(String roleid, String rolenm,
								String sortorder);

    int getRoleInfosCount(String roleid, String rolenm);

    void addRoleInfo(RoleInfo instance);

    void modRoleInfo(String roleid, String rolenm, String lmtserial);

    void delRoleInfo(String roleid);

}
