package cn.demoframe.test.frame.service;

import cn.demoframe.test.frame.model.OprLog;

import java.util.List;

public interface OprLogService {

    List<OprLog> getOprLogs(String opuserid, String optype, String sortorder);

    int getOprLogsCount(String opuserid, String optype);

    void addOprLog(OprLog instance);
}
