package cn.demoframe.test.frame.service.impl;

import cn.demoframe.test.core.dao.CommonDao;
import cn.demoframe.test.frame.model.RoleInfo;
import cn.demoframe.test.frame.service.RoleinfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("RoleinfoServiceImpl")
public class RoleinfoServiceImpl implements RoleinfoService {

    @Resource
    private CommonDao commonDao;

    public void addRoleInfo(RoleInfo instance) {
    	commonDao.insertInfo("insertRoleinfo", instance);
    }

    public void delRoleInfo(String roleid) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("roleid", roleid);
        commonDao.deleteInfo("deleteRoleinfo", index);
    }

    public List<RoleInfo> getRoleInfos(String roleid, String rolenm, 
            String sortorder) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("roleid", roleid);
        index.put("rolenm", rolenm);
        index.put("sortorder", sortorder);

        return commonDao.selectInfo("selectRoleinfos", index);
    }

    public int getRoleInfosCount(String roleid, String rolenm) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("roleid", roleid);
        index.put("rolenm", rolenm);
        return commonDao.selectInfoCount("selectRoleinfosCount", index);
    }

    public void modRoleInfo(String roleid, String rolenm, String lmtserial) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("roleid", roleid);
        index.put("rolenm", rolenm);
        index.put("lmtserial", lmtserial);
        commonDao.updateInfo("updateRoleinfo", index);
    }

}
