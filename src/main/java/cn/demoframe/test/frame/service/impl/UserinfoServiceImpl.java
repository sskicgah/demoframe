package cn.demoframe.test.frame.service.impl;

import cn.demoframe.test.core.dao.CommonDao;
import cn.demoframe.test.frame.model.UserInfo;
import cn.demoframe.test.frame.service.UserinfoService;
import cn.demoframe.test.utils.Constants;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("UserinfoServiceImpl")
public class UserinfoServiceImpl implements UserinfoService {

	@Resource
	private CommonDao commonDao;

    public void addUserInfo(UserInfo instance) {
    	commonDao.insertInfo("insertUserinfo", instance);
    }
    
    public void addUserInfos(List<UserInfo> instances) {
    	commonDao.insertInfos("insertUserinfo", instances);
    }

    public void delUserInfo(String userid) {
        Map<String, Object> index = new HashMap<String, Object>();
        if (userid != null){
        	index.put("userid", userid);
        	index.put("status", Constants.STATUS_OFF);
        }
        commonDao.updateInfo("updateUserinfo", index);
    }

    public List<UserInfo> getUserInfos(String parentid, String userid, String usernm, String passwd, String roleid, Integer status, Integer startRow, Integer pageSize, String sortorder) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("parentid", parentid);
        index.put("userid", userid);
        index.put("usernm", usernm);
        index.put("passwd", passwd);
        index.put("roleid", roleid);
        index.put("status", status);
        index.put("sortorder", sortorder);
        index.put("startRow", startRow);
        index.put("pageSize", pageSize);
        
        return commonDao.selectInfo("selectUserinfos", index);
    }

    public int getUserInfosCount(String userid, String usernm, String passwd, String roleid, Integer status) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("userid", userid);
        index.put("usernm", usernm);
        index.put("passwd", passwd);
        index.put("roleid", roleid);
        index.put("status", status);

        return commonDao.selectInfoCount("selectUserinfosCount", index);
    }

    public void modUserInfo(String userid, String usernm, String passwd,
                            String roleid, String email, String mobile, String remark, Integer status) {
        Map<String, Object> index = new HashMap<String, Object>();
        index.put("userid", userid);
        index.put("usernm", usernm);
        index.put("passwd", passwd);
        index.put("roleid", roleid);
        index.put("email", email);
        index.put("mobile", mobile);
        index.put("remark", remark);
        index.put("status", status);
        commonDao.updateInfo("updateUserinfo", index);
    }

}
