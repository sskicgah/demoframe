package cn.demoframe.test.core.dao;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Repository
public class LogDao extends BaseDaoImpl {
	@Resource(name = "logSqlSessionFactory")
	private SqlSessionFactory sqlSessionFactory;

	@PostConstruct
	public void setSqlSessionFactory() {
		SqlSessionFactory(sqlSessionFactory);
	}
}
