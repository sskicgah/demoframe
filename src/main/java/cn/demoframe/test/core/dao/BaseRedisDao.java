package cn.demoframe.test.core.dao;

import java.util.Set;

public interface BaseRedisDao {

	/**
	 * 设置值
	 * @param key 键
	 * @param value 值
	 */
	void set(final String key, final String value);
	
	/**
	 * 设置key和有效时间
	 * */
	void setex(final String key, final String value, final Integer seconds);

	/**
	 * 获取值
	 * @param key 键
	 * @return 值
	 */
	String get(final String key);
	
	/**
	 * 删除
	 * @param key 键
	 */
	void delete(final String key);

	/**
	 * 删除全部相关键值
	 * @param pattern 前缀，方便统一删除
	 */
	void clear(final String pattern);

	Set<String> getKeys(final String pattern);
	
	/**
	 * 判断key是否存在
	 * */
	boolean exists(final String key);
	
	/**
	 * 获取key的过期时间
	 * */
	Long getExpire(String key);

}
