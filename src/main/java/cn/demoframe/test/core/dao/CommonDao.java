package cn.demoframe.test.core.dao;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Repository
public class CommonDao extends BaseDaoImpl {

	@Resource(name = "sqlSessionFactory")
	private SqlSessionFactory sqlSessionFactory;

	@PostConstruct
	public void setSqlSessionFactory() {
		SqlSessionFactory(sqlSessionFactory);
	}

}
