package cn.demoframe.test.core.dao;

import java.util.List;
import java.util.Map;

public interface BaseDao {

	<T> List<T> selectInfo(String opType, Map<String, Object> index);

	int selectInfoCount(String opType, Map<String, Object> index);

	<T> int insertInfo(String opType, T instance);

	<T> int insertInfos(String statement, List<T> list);

	int updateInfo(String opType, Map<String, Object> index);

	int updateInfos(String opType, List<?> index);

	int deleteInfo(String opType, Map<String, Object> index);

	int deleteInfos(String opType, List<?> index);
}
