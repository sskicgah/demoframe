package cn.demoframe.test.core.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import cn.demoframe.test.utils.Constants;

public class BaseDaoImpl extends SqlSessionDaoSupport implements BaseDao {

	private SqlSessionFactory sqlSessionFactory;

	public void SqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		super.setSqlSessionFactory(sqlSessionFactory);
		this.sqlSessionFactory = sqlSessionFactory;
	}

	public int batchInsert(String statement, List<?> list) {
		if(list == null || list.size() == 0) {
			return 0;
		}
		SqlSession batchSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
		int i = 0;
		for(int cnt = list.size(); i < cnt; i++) {
			batchSession.insert(statement, list.get(i));
			if((i + 1) % Constants.BATCH_DEAL_NUM == 0 || i == cnt - 1) {
				batchSession.flushStatements();
			}
		}
		batchSession.flushStatements();
		batchSession.close();
		return i;
	}

	public int batchUpdate(String statement, List<?> list) {
		if(list == null || list.size() == 0) {
			return 0;
		}
		SqlSession batchSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
		int i = 0;
		for(int cnt = list.size(); i < cnt; i++) {
			batchSession.update(statement, list.get(i));
			if((i + 1) % Constants.BATCH_DEAL_NUM == 0) {
				batchSession.flushStatements();
			}
		}
		batchSession.flushStatements();
		batchSession.close();
		return i;
	}

	public int batchDelete(String statement, List<?> list) {
		if(list == null || list.size() == 0) {
			return 0;
		}
		SqlSession batchSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
		int i = 0;
		for(int cnt = list.size(); i < cnt; i++) {
			batchSession.delete(statement, list.get(i));
			if((i + 1) % Constants.BATCH_DEAL_NUM == 0) {
				batchSession.flushStatements();
			}
		}
		batchSession.flushStatements();
		batchSession.close();
		return i;
	}

	public <T> int insertInfo(String opType, T instance) {
		if(instance == null) {
			return 0;
		}
		return this.getSqlSession().insert(opType, instance);
	}

	public int deleteInfo(String opType, Map<String, Object> index) {
		return this.getSqlSession().delete(opType, index);
	}

	public int updateInfo(String opType, Map<String, Object> index) {
		return this.getSqlSession().update(opType, index);
	}

	public <T> List<T> selectInfo(String opType, Map<String, Object> index) {
		return this.getSqlSession().selectList(opType, index);
	}

	public int selectInfoCount(String opType, Map<String, Object> index) {
		return (Integer)this.getSqlSession().selectOne(opType, index);
	}

	public <T> int insertInfos(String opType, List<T> list) {
		return batchInsert(opType, list);
	}

	public int updateInfos(String opType, List<?> index) {
		return batchUpdate(opType, index);
	}

	public int deleteInfos(String opType, List<?> list) {
		return batchDelete(opType, list);
	}
}
