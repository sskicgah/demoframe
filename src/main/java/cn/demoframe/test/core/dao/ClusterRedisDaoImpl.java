package cn.demoframe.test.core.dao;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

public class ClusterRedisDaoImpl implements BaseRedisDao {

	private JedisCluster jedisCluster;

	public void setJedisCluster(JedisCluster jedisCluster) {
		this.jedisCluster = jedisCluster;
	}

	public ClusterRedisDaoImpl() {
	}

	/**
	 * 设置key
	 * */
	public void set(String key, String value) {
		if(StringUtils.isEmpty(key)) {
			return;
		}
		if(value == null) {
			value = "";
		}
		jedisCluster.set(key, value);
	}
	
	/**
	 * 设置key和有效时间
	 * */
	public void setex(String key, String value, Integer seconds) {
		if(StringUtils.isEmpty(key) || seconds == null) {
			return;
		}
		jedisCluster.setex(key, seconds, value);
	}

	/**
	 * 获取key对应值
	 * */
	public String get(String key) {
		if(StringUtils.isEmpty(key)) {
			return "";
		}
		return jedisCluster.get(key);
	}

	/**
	 * 设置失效时长，单位秒
	 * */
	public void expireAt(String key, Long unixTime) {
		if(StringUtils.isEmpty(key) || unixTime == null) {
			return;
		}
		jedisCluster.expireAt(key, unixTime);
	}

	/**
	 * 设置失效时长，单位秒
	 * */
	public void expire(String key, Integer seconds) {
		if(StringUtils.isEmpty(key) || seconds == null) {
			return;
		}
		jedisCluster.expire(key, seconds);
	}

	/**
	 * 删除key
	 * */
	public void delete(final String key) {
		if(StringUtils.isEmpty(key)) {
			return;
		}
		jedisCluster.del(key);
	}

	/**
	 * 批量删除满足前缀的key
	 * */
	public void clear(String pattern) {
		if(StringUtils.isEmpty(pattern)) {
			return;
		}
		Set<String> keys = getKeys(pattern);
		Iterator<String> it = keys.iterator();  
        while(it.hasNext()){  
            String keyStr = it.next();  
            this.delete(keyStr);  
        }  
	}

	/**
	 * 获取满足前缀的key
	 * */
	public Set<String> getKeys(String pattern) {
		if(StringUtils.isEmpty(pattern)) {
			return null;
		}
		TreeSet<String> keys = new TreeSet<>();  
        Map<String, JedisPool> clusterNodes = jedisCluster.getClusterNodes();  
        for(String node : clusterNodes.keySet()){  
            JedisPool jp = clusterNodes.get(node);  
            Jedis connection = null;  
            try {
            	connection = jp.getResource();
                keys.addAll(connection.keys(pattern+"*"));  
            } catch(Exception e){  
                System.out.println("Getting keys error");
                e.printStackTrace();
            } finally{  
                connection.close();//用完一定要close这个链接！！！  
            }  
        }  
        return keys; 
	}

	/**
	 * 是否存在key
	 * */
	public boolean exists(String key) {
		if(StringUtils.isEmpty(key)) {
			return false;
		}
		return jedisCluster.exists(key);
	}

	public Long getExpire(String key, TimeUnit unit) {
		return null;
	}

	public Long getExpire(String key) {
		if (StringUtils.isEmpty(key)) {
			return null;
		}
		return jedisCluster.ttl(key);
	}

}
