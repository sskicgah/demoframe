package cn.demoframe.test.core.dao;

import org.springframework.stereotype.Repository;
import redis.clients.jedis.JedisCluster;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Repository
public class RedisDao extends ClusterRedisDaoImpl {

	@Resource(name = "jedisCluster")
	private JedisCluster jedisCluster;

	@PostConstruct
	public void setJedisCluster() {
		setJedisCluster(jedisCluster);
	}
}
