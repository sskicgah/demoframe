package cn.demoframe.test.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeUtility;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;

public class MailToolkit {

	private String host;
	private String username;
	private String password;
	private String email;
	
	public static void main(String[] args) {
		MailToolkit mail = new MailToolkit(Constants.MAIL_HOST, Constants.MAIL_USER, Constants.MAIL_PWD, Constants.MAIL_EMAIL);
		List<String> sendTos = new ArrayList<String>();
		sendTos.add("test@qq.com");
//		sendTos.add("test@163.cn");
//		mail.sendMail(sendTos, "测试标题", "测试邮件");
		Map<String, String> attachs = new HashMap<String, String>();
		attachs.put("test", "D:/test");
		attachs.put("test2", "D:/test2");
		attachs.put("test3", "D:/test3");
		mail.sendMutiMail(sendTos, "测试标题", "测试邮件", attachs);
	}
	
	/**
	 * 初始化发送账号信息
	 * @param host 邮件服务器域名
	 * @param username 用户名
	 * @param password 密码
	 * @param email 发送账号邮件地址
	 */
	public MailToolkit(String host, String username, String password, String email) {
		super();
		this.host = host;
		this.username = username;
		this.password = CryptToolkit.base643DesDecoder(password, Constants.ENCRYPTKEY);
		this.email = email;
	}

	/**
	 * 发送简单邮件
	 * @param sendTos 收件人列表
	 * @param subject 邮件标题
	 * @param content 邮件内容
	 */
	public void sendMail(List<String> sendTos, String subject, String content) {
		SimpleEmail mail = new SimpleEmail();
		mail.setHostName(host);
		mail.setAuthentication(username, password);
		try {
			for(String sendTo : sendTos) {
				mail.addTo(sendTo, "");
			}
			mail.setFrom(email, "");
			mail.setSubject(subject);
			mail.setMsg(content);
			mail.setCharset("UTF-8");
			mail.send();
		} catch (EmailException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 发送含有附件的邮件
	 * @param sendTos 收件人列表
	 * @param subject 邮件标题
	 * @param content 邮件内容
	 * @param attachs 附件列表，按照map(文件名,文件路径)设置
	 */
	public void sendMutiMail(List<String> sendTos, String subject, String content, Map<String, String> attachs) {
		MultiPartEmail mail = new MultiPartEmail();
		mail.setHostName(host);
		mail.setAuthentication(username, password);
		try {
			for(String sendTo : sendTos) {
				mail.addTo(sendTo, "");
			}
			mail.setFrom(email, "");
			mail.setSubject(subject);
			mail.setMsg(content);
			mail.setCharset("UTF-8");
			EmailAttachment attachment = null;
			for(String name : attachs.keySet()) {
				attachment = new EmailAttachment();
				attachment.setDisposition(EmailAttachment.ATTACHMENT);
				attachment.setPath(attachs.get(name));
				attachment.setName(MimeUtility.encodeText(name));
				mail.attach(attachment);
			}
			mail.send();
		} catch (EmailException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
