package cn.demoframe.test.utils;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Constants {
	private static final Logger logger = LoggerFactory.getLogger(Thread
			.currentThread().getStackTrace()[1].getClassName());

	//系统配置文件
	private static PropertiesConfiguration config;
	private static String curEnv = "";

	static{
		try {
			config = new PropertiesConfiguration();
			config.setEncoding("UTF-8");
			config.load("config.properties");
			config.setReloadingStrategy(new FileChangedReloadingStrategy());
			curEnv = config.getString("curEnv");
		} catch (ConfigurationException e) {
			logger.error("找不到配置文件", e);
		}
	}

	private static String getString(String key) {
		if(config.containsKey(key + curEnv)) {
			return config.getString(key + curEnv);
		}
		return config.getString(key);
	}

	private static String[] getStringArray(String key) {
		if(config.containsKey(key + curEnv)) {
			return config.getStringArray(key + curEnv);
		}
		return config.getStringArray(key);
	}

	private static Integer getInt(String key) {
		if(config.containsKey(key + curEnv)) {
			return config.getInt(key + curEnv);
		}
		return config.getInt(key);
	}

	// 批量提交数
	public final static int BATCH_DEAL_NUM = 2000;

	//状态开关
	public static final int STATUS_ON = 1;
	public static final int STATUS_OFF = 0;

	// 功能节点数上限
	public final static int NODE_NUM_MAX = 50;
	// 加密密钥
	public final static String ENCRYPTKEY = "3dEs+fDd#14%pWd64";
	// http超时字符串
	public final static String HTTP_TIMEOUT_STR = "__**/HTTP timeout__*_*";


	// 不拦截关键字
	public final static String[] NO_FILTER = getStringArray("no_fiter");

	// 邮件设置
	public final static String MAIL_HOST = getString("mail.host");
	public final static String MAIL_USER = getString("mail.username");
	public final static String MAIL_PWD = getString("mail.password");
	public final static String MAIL_EMAIL = getString("mail.email");

	public static String getTestStr() {
		return getString("test.str");
	}

}
