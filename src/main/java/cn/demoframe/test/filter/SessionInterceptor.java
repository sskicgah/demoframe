package cn.demoframe.test.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.demoframe.test.utils.Constants;

public class SessionInterceptor extends HandlerInterceptorAdapter {

    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {
    	response.setCharacterEncoding("UTF-8");
    	String uri = request.getRequestURI();
    	boolean needLogin = false;
    	for(String s : Constants.NO_FILTER) {
    		if(!uri.contains(s)) {
    			needLogin = true;
    		} else {
    			needLogin = false;
    			break;
    		}
    	}
    	if(uri.contains("login.do")) {
    		needLogin = false;
    	}
    	if(needLogin) {
    		Object userName = request.getSession().getAttribute("LOGIN_USER");
    		if(userName == null) {
    			response.setHeader("sessionstatus", "timeout");
    			System.out.println("会话超时");
    			return false;
    		}
    	}
		return super.preHandle(request, response, handler);
	}
}
