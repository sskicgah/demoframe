package cn.demoframe.test.model;

import java.util.List;

public class Griddata_footer {
	public Griddata_footer() {
		super();
	}
	public Griddata_footer(List<?> rows, List<?> footer) {
		super();
		this.rows = rows;
		this.footer = footer;
	}
	private int code = 0;
	private String msg = "";
	private List<?> rows;
	private List<?> footer;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<?> getRows() {
		return rows;
	}
	public void setRows(List<?> rows) {
		this.rows = rows;
	}
	public List<?> getFooter() {
		return footer;
	}
	public void setFooter(List<?> footer) {
		this.footer = footer;
	}
}
