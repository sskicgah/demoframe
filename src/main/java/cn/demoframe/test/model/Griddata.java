package cn.demoframe.test.model;

import java.util.List;

public class Griddata {
	public Griddata() {
		super();
	}
	public Griddata(List<?> rows) {
		super();
		this.rows = rows;
	}
	private List<?> rows;
	public List<?> getRows() {
		return rows;
	}
	public void setRows(List<?> rows) {
		this.rows = rows;
	}
}
