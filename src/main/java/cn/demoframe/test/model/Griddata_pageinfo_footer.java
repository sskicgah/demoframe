package cn.demoframe.test.model;

import java.util.List;

public class Griddata_pageinfo_footer {

	public Griddata_pageinfo_footer() {
		super();
	}
	public Griddata_pageinfo_footer(int total, List<?> rows, List<?> footer) {
		super();
		this.rows = rows;
		this.footer = footer;
		this.total = total;
	}
	private List<?> rows;
	private List<?> footer;
	private int total;
	public List<?> getRows() {
		return rows;
	}
	public void setRows(List<?> rows) {
		this.rows = rows;
	}
	public List<?> getFooter() {
		return footer;
	}
	public void setFooter(List<?> footer) {
		this.footer = footer;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "griddata_pageinfo_footer [footer=" + footer + ", rows=" + rows
				+ ", total=" + total + "]";
	}
}
