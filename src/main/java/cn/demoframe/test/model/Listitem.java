package cn.demoframe.test.model;

public class Listitem {
    private String text;
    private String id;
    private boolean selected = false;
	public Listitem() {
		super();
	}
	public Listitem(String text, String id) {
		super();
		this.text = text;
		this.id = id;
	}
	public Listitem(String text, String id, boolean selected) {
		super();
		this.text = text;
		this.id = id;
		this.selected = selected;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
    
}
