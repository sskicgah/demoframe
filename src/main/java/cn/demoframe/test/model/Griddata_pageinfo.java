package cn.demoframe.test.model;

import java.util.List;

public class Griddata_pageinfo {
	public Griddata_pageinfo() {
		super();
	}
	public Griddata_pageinfo(int total, List<?> rows) {
		super();
		this.total = total;
		this.rows = rows;
	}
	private int code = 0;
	private String msg = "";
	private int total;
	private List<?> rows;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List<?> getRows() {
		return rows;
	}
	public void setRows(List<?> rows) {
		this.rows = rows;
	}
}
