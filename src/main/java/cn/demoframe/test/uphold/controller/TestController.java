package cn.demoframe.test.uphold.controller;

import cn.demoframe.test.utils.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Controller
public class TestController {

	private Queue<String> queues = new ConcurrentLinkedQueue<String>();

	private boolean start = true;

	@RequestMapping(value = { "/api/addTest/{rec}" }, method = { RequestMethod.GET })
	@ResponseBody
	public String addTest(HttpServletRequest request,
    	@PathVariable(value = "rec") String rec) {
		queues.add(rec);
		return "ok";
	}

	@RequestMapping(value = { "/api/getTest" }, method = { RequestMethod.GET })
	@ResponseBody
	public Integer getTest(HttpServletRequest request) {
		start = false;
		return 0;
//		return queues.size();
	}

	@RequestMapping(value = { "/api/clearTest" }, method = { RequestMethod.GET })
	@ResponseBody
	public String clearTest(HttpServletRequest request) {
//		while(!queues.isEmpty()) {
//			System.out.println(queues.poll());
//		}

		while (start) {
			System.out.println("in while ...");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return "ok! " + queues.size();
	}

	@RequestMapping(value = { "/api/reloadTest" }, method = { RequestMethod.GET })
	@ResponseBody
	public String reloadTest() {
		return Constants.getTestStr();
	}
}
