var web_path = '';
//全局的ajax访问，处理ajax清求时sesion超时
function init($) {
    $.ajaxSetup({
        contentType:'application/x-www-form-urlencoded;charset=utf-8',
        complete:function(XMLHttpRequest,textStatus){
            var sessionstatus = XMLHttpRequest.getResponseHeader('sessionstatus');//通过XMLHttpRequest取得响应头，sessionstatus，
            if(sessionstatus == 'timeout'){
                //如果超时就处理 ，指定要跳转的页面
                top.location = web_path == '' ? '/' : web_path;
            }
        }
    });
}
//表单序列化
$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
		if (!o[this.name].push) {
			o[this.name] = [ o[this.name] ];
		}
		o[this.name].push(this.value || '');
	    } else {
			o[this.name] = this.value || '';
	    }
	});
	return o;
};

//保留两位小数(非强制)
function changeTwoDecimal(x) {
	var f_x = parseFloat(x);
	if (isNaN(f_x))
	{
		//alert('function:changeTwoDecimal->parameter error');
		return false;
	}
	f_x = Math.round(x*100)/100;
	return f_x;
}
//保留两位小数(强制)
function changeTwoDecimal_f(x) {
	var f_x = parseFloat(x);
	if (isNaN(f_x))
	{
		alert('function:changeTwoDecimal->parameter error');
		return false;
	}
	f_x = Math.round(x*100)/100;
	var s_x = f_x.toString();
	var pos_decimal = s_x.indexOf('.');
	if (pos_decimal < 0)
	{
		pos_decimal = s_x.length;
		s_x += '.';
	}
	while (s_x.length <= pos_decimal + 2)
	{
		s_x += '0';
	}
	return s_x;
}
//获取url参数
function getUrlParam(id){
	var reg = new RegExp('(^|&)' + id + '=([^&]*)(&|$)', 'i');
	var r = window.location.search.substr(1).match(reg);
	if(r != null)
		return decodeURI(r[2]);
	return null;
}
//前端分页过滤器,easyui 1.3.1可用
function pagerFilter(data){
	if(data.total == undefined){
		data.total = data.rows.length;
	}
	if(data.backuprows == undefined){
		data.backuprows = data.rows;
	}
	var dg = $(this);
	var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage:function(pageNum, pageSize){
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh',{
                pageNumber:pageNum,
                pageSize:pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    var start = (opts.pageNumber-1)*parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = data.backuprows.slice(start, end);
    return data;
}

function lAlert(layer, msg) {
    layer.alert(msg, {offset: '100px'});
}

function lMsg(layer, msg) {
    layer.msg(msg, {offset: '100px'});
}

function loadCombo($, id, data, selectedid) {
	var str = '';
	for (var i = 0; i < data.length; i++) {
		str += '<option value="'+data[i].id+'" '+ (data[i].id == selectedid ? 'selected="selected"':'') + '>' + data[i].text + '</option>';
	}
	$('#' + id).append(str);
}