<%@page contentType="text/html;charset=UTF-8" language="java" errorPage="" %>
<%@include file="../../frame/getTime.jsp"%>
<%
	String realPath = application.getRealPath("/");
	request.setAttribute("ctx", request.getContextPath());
	request.setAttribute("t", getTime(realPath, "/js/common.js", "/view/content/auth/js/roles.js"));
%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="${ctx}/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
	<div class="layui-container">
		<div class="layui-col-md4">
			<legend>角色信息</legend>
			<div class="layui-btn-group">
				<button id="addRole" class="layui-btn"><i class="layui-icon">&#xe61f;</i>新增角色</button>
			</div>
			<table class="layui-table" id="datagrid" lay-filter="role"></table>
		</div>
		<div id="treeDiv" class="layui-col-md8" style="display: none;">
			<legend>角色权限</legend>
			<div class="layui-btn-group">
				<button id="saveRole" class="layui-btn"><i class="layui-icon">&#xe618;</i>保存</button>
				<button id="refresh" class="layui-btn"><i class="layui-icon">&#x1002;</i>刷新</button>
			</div>
			<ul id="lmttree" style="margin:10px"></ul>
		</div>
	</div>

	<script type="text/html" id="bar">
		{{# if(d.roleid > 0) { }}
		<a class="layui-btn layui-btn-danger layui-btn-mini" lay-event="del">删除</a>
		{{#  } }}
	</script>
</div>
<script type="text/javascript" src="${ctx}/layui/layui.js"></script>
<script type="text/javascript" src="${ctx}/layui/tree2.js"></script>
<script type="text/javascript" src="${ctx}/js/common.js?v=${t[0]}"></script>
<script type="text/javascript" src="js/roles.js?v=${t[1]}"></script>
</body>
</html>