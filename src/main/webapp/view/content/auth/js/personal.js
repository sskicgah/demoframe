layui.use(['jquery', 'layer'], function(){
	var $ = layui.$,
        layer = layui.layer;

    init($);
    var initDialog = function() {
        layer.open({
        	type: 1,
            title: '提示信息',
            id: 'changePwdDialog',
			offset: '100px',
            area: ['430px', '290px'],
            content: '<div id="edit-dialog" style="width:360px;height:165px;"><div class="layui-form-item" style="margin-top: 10px;"><label class="layui-form-label">旧密码:</label><div class="layui-input-block"><input id="oldpasswd" type="password" placeholder="请输入旧密码" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">新密码:</label><div class="layui-input-block"><input id="newpasswd" type="password" placeholder="请输入新密码" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">新密码:</label><div class="layui-input-block"><input id="newpasswd2" type="password" placeholder="请再次输入密码" class="layui-input"></div></div></div>',
            btn: ['确定', '取消'],
            yes: function(index) {
                if ($('#oldpasswd').val() == '') {
                    lAlert(layer, '您尚未输入旧密码!');
                } else if ($('#newpasswd').val() == '') {
                    lAlert(layer, '您尚未输入新密码!');
                } else if ($('#newpasswd2').val() == '') {
                    lAlert(layer, '请再次输入新密码!');
                } else if ($('#newpasswd').val() != $('#newpasswd2').val()) {
                    lAlert(layer, '两次输入的新密码不一致!');
                } else {
                    layer.confirm('您是否确认提交?', {btn: ['确认', '取消'], offset: '100px'}, function () {
                        $.post(web_path + '/auth/userinfo/modpasswd.do', {
                            oldpasswd: $('#oldpasswd').val(),
                            newpasswd: $('#newpasswd').val()
                        }, function (data) {
                            lMsg(layer, data.detail);
                            if (data.result == '00') {
                                layer.close(index);
                            }
                        }).error(function () {
                            lMsg(layer, '密码修改失败.');
                        });//个人密码修改 end
                    });
                }
            }
        });
    }, setPersonalInfo = function (data) {
        $('#userid').html(data.userid);
        $('#usernm').val(data.usernm);
        $('#rolenm').html(data.rolenm);
        $('#email').val(data.email);
        $('#mobile').val(data.mobile);
        $('#remark').html(data.remark);
    };

    $.post(web_path + '/auth/userinfo/getpersonal.do',
        function(data){
            setPersonalInfo(data);
        });
    $('#btn-refresh').click(function(){
        $.post(web_path + '/auth/userinfo/getpersonal.do',
            function(data){
                setPersonalInfo(data);
            });
    });
    $('#btn-save').click(function(){
        $.post(web_path + '/auth/userinfo/modpersonal.do',{
            usernm : $('#usernm').val(),
            email : $('#email').val(),
            mobile : $('#mobile').val(),
            remark : $('#remark').html()
        }, function(data){
            layer.msg(data.detail, {offset:'200px'});
        }).error(function(){
            layer.msg('个人信息修改失败.', {offset:'200px'});
        });//个人信息修改 end
    });
    $('#btn-repasswd').click(function(){
        initDialog();
    });
});