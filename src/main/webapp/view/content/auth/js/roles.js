layui.use(['jquery', 'table', 'layer', 'form'], function() {
    var tObj, tree, selected_id;
    var $ = layui.$,
        table = layui.table,
        layer = layui.layer,
        form = layui.form;

    init($);
    var initTable = function() {
        tObj = table.render({
            elem: '#datagrid',
            height: 'full-105',
            url: web_path + '/auth/roleinfo/getroleinfos.do',
            method: 'post',
            response: {
                countName: 'total',
                dataName: 'rows'
            },
            cols: [[
                {field:'roleid',title:'角色标识',width:90, event: 'show'},
                {field:'rolenm',title:'角色名称',width:140, event: 'show'},
                {fixed: 'right', title: '操作', width: 60, align: 'center', toolbar: '#bar'}
            ]]
        });
    }, tableReload = function () {
        tObj.reload();
    }, initDialog = function() {
        layer.open({
            type: 1,
            title: '角色信息',
            id: 'addRoleInfo',
            offset: '10px',
            area: ['360px', '240px'],
            content: '<form class="layui-form" style="width:340px;height:85px;"><div class="layui-form-item" style="margin-top: 10px;"><label class="layui-form-label">角色标识：</label><div class="layui-input-block"><input id="roleid" type="text" placeholder="请输入角色ID" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">角色名称：</label><div class="layui-input-block"><input id="rolenm" type="text" placeholder="请输入角色名称" class="layui-input"></div></div></form>',
            btn: ['确定', '取消'],
            yes: function(index) {
                if ($('#roleid').val() == '') {
                    lAlert(layer, '您尚未输入角色ID!');
                } else if ($('#rolenm').val() == '') {
                    lAlert(layer, '您尚未输入角色名称!');
                } else {
                    var url = web_path + '/auth/roleinfo/addrole.do';
                    layer.confirm('您是否确认提交?', {btn: ['确认', '取消'], offset: '100px'}, function () {
                        $.ajax({
                            url: url,
                            type: 'POST',
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8',
                            data: JSON.stringify({
                                roleid: $('#roleid').val(),
                                rolenm: $('#rolenm').val()
                            }),
                            success: function(data){
                                lMsg(layer, data.detail);
                                if (data.result == '00') {
                                    tableReload();
                                    layer.close(index);
                                }
                            },
                            error:function () {
                                lMsg(layer, '新增失败!');
                            }
                        });
                    });
                }
            },
            cancel: function (index) {
                layer.close(index);
            }
        });
    }, initTree = function (roleid) {
        selected_id = roleid;
		$.post(web_path + '/auth/roleinfo/getrolenodes.do', {
            roleid : roleid
		}, function (data) {
            $('#treeDiv').show();
            if (tree) {
                $('#lmttree').html('');
            }
            tree = layui.tree({
                elem: '#lmttree',
                check:"checkbox",
                nodes: data
            });
        });
    };

    initTable();
    table.on('tool(role)', function(obj){
        var data = obj.data;
        if(obj.event === 'del') {
            layer.confirm('确定要删除？', function (index) {
                layer.close(index);
                $.post(web_path + '/auth/roleinfo/delrole.do', {
                    roleid : data.roleid
                }, function(data) {
                    obj.del();
                    $('#treeDiv').hide();
                    lMsg(layer, data.detail);
                });
            });
        } else if (obj.event === 'show') {
        	initTree(data.roleid);
		}
    });
    $('#addRole').click(function(){
        initDialog();
    });
    $('#saveRole').click(function() {
        var nodes = tree.getChecked();
        var ids = '';
        for (var i = 0; i < nodes.length; i++) {
            if (ids != '') ids += ',';
            ids += nodes[i].id;
        }
        if (ids == '') {
            lAlert(layer, '请至少勾选一个功能。');
        } else {
            $.ajax({
                type: 'POST',
                url: web_path + '/auth/roleinfo/modrole.do',
                data: {
                    roleid: selected_id,
                    ids: ids
                },
                dataType: 'json',
                success: function (data) {
                    lMsg(layer, data.detail);
                },
                error: function (data) {
                    lMsg(layer, '角色权限修改失败.');
                }
            });
        }
    });
    $('#refresh').click(function(){
        initTree(selected_id);
    });
});