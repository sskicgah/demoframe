layui.use(['jquery', 'table', 'layer', 'form'], function(){
    var tObj;
    var $ = layui.$,
        table = layui.table,
        layer = layui.layer,
        form = layui.form;

    init($);
    var initTable = function() {
        tObj = table.render({
            elem: '#datagrid',
            height: 'full-105',
            url: web_path + '/auth/menuinfo/getmenuinfos.do',
            method: 'post',
            response: {
                countName: 'total',
                dataName: 'rows'
            },
            cols: [[
                {field: 'menuid', title: '菜单ID', width: 80},
                {field: 'title', title: '标题', width: 200},
                {field: 'nodetype', title: '节点类型', width: 90, templet: '#nodeTpl'},
                {field: 'position', title: '显示位置', width: 90},
                {field: 'parentid', title: '父节点ID', width: 90},
                {field: 'url', title: 'URL', width: 300},
                {fixed: 'right', title: '操作', width: 140, align: 'center', toolbar: '#bar'}
            ]]
        });
    }, tableReload = function () {
        tObj.reload();
    }, initDialog = function(type, rowData) {
        layer.open({
            type: 1,
            title: '菜单信息',
            id: 'editMenuInfo',
            offset: '10px',
            area: ['430px', '440px'],
            content: '<form class="layui-form" style="width:360px;height:165px;"><div class="layui-form-item" style="margin-top: 10px;"><label class="layui-form-label">菜单ID：</label><div class="layui-input-block"><input id="menuid" type="text" placeholder="请输入菜单ID" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">标题：</label><div class="layui-input-block"><input id="title" type="text" placeholder="请输入标题" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">节点类型：</label><div class="layui-input-block"><select id="nodetype" lay-filter="nodetype"'+ (type == 1 ? ' disabled':'') +'><option value="0">父节点</option><option value="1" selected="selected">子节点</option></select></div></div><div class="layui-form-item"><label class="layui-form-label">父节点ID：</label><div class="layui-input-block"><input id="parentid" type="text" placeholder="请输入父节点ID" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">显示位置：</label><div class="layui-input-block"><input id="position" type="text" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">URL：</label><div class="layui-input-block"><input id="url" type="text" placeholder="请输入URL" class="layui-input"></div></div></form>',
            btn: ['确定', '取消'],
            yes: function(index) {
            	var nodetype = $('#nodetype').val();
                if ($('#menuid').val() == '') {
                    lAlert(layer, '您尚未输入菜单ID!');
                } else if ($('#title').val() == '') {
                    lAlert(layer, '您尚未输入菜单标题!');
                } else if (nodetype == '1' && $('#url').val() == '') {
                    lAlert(layer, '您尚未输入URL!');
                } else {
                    var url = web_path + '/auth/menuinfo/addmenu.do';
                    if (type == 1) {
                        url = web_path + '/auth/menuinfo/modmenu.do';
                    }
                    layer.confirm('您是否确认提交?', {btn: ['确认', '取消'], offset: '100px'}, function () {
                        $.post(url, {
                            menuid : $('#menuid').val(),
                            title : $('#title').val(),
                            nodetype : nodetype,
                            position : $('#position').val(),
                            parentid : nodetype=='1' ? $('#parentid').val() : '-1',
                            url : nodetype=='1' ? $('#url').val() : ' '
                        }, function (data) {
                            lMsg(layer, data.detail);
                            if (data.result == '00') {
                                tableReload();
                                layer.close(index);
                            }
                        }).error(function(){
                            if (type == 0) {
                                lMsg(layer, '新增失败!');
                            } else if (type == 1) {
                                lMsg(layer, '修改失败!');
                            }
                        });
                    });
                }
            },
            cancel: function (index) {
                layer.close(index);
            },
            success: function () {
                form.render('select');
                if (type == 1) {
                    setTableData(rowData);
                }
            }
        });
    }, setTableData = function(data) {
        $('#menuid').val(data.menuid);
        $('#title').val(data.title);
        $('#nodetype').get(0).selectedIndex = data.nodetype;
        $('#position').val(data.position);
        $('#parentid').val(data.parentid);
        $('#url').val(data.url);
        if (data.nodetype == '0') {
            $('#parentid').attr('readonly', true);
        }
    };

    initTable();
    table.on('tool(menu)', function(obj){
        var data = obj.data;
        if(obj.event === 'edit'){
            initDialog(1, data);
        } else if(obj.event === 'del') {
            layer.confirm('确定要删除？', function (index) {
                layer.close(index);
                $.post(web_path + '/auth/menuinfo/delmenu.do', {
                    menuid : data.menuid
                }, function(data) {
                    obj.del();
                    lMsg(layer, data.detail);
                });
            });
        }
    });
    form.on('select(nodetype)', function (data) {
		if (data.value == '0') {
            $('#parentid').attr('readonly', true);
		} else {
            $('#parentid').attr('readonly', false);
		}
    });
    $('#addMenu').click(function(){
        initDialog(0);
    });
})