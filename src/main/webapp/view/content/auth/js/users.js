layui.use(['jquery', 'table', 'layer', 'form'], function(){
	var comboData = null, tObj;
    var $ = layui.$,
    	table = layui.table,
		layer = layui.layer,
		form = layui.form;

    init($);
    var initTable = function() {
        tObj = table.render({
            elem: '#datagrid',
            height: 'full-105',
            url: web_path + '/auth/userinfo/userinfos.do',
            method: 'post',
            response: {
                countName: 'total',
                dataName: 'rows'
            },
            page: true,
            cols: [[
                {field: 'userid', title: '用户ID', width: 100},
                {field: 'usernm', title: '用户姓名', width: 100},
                {field: 'rolenm', title: '角色名称', width: 100},
                {field: 'email', title: '电子邮件', width: 160},
                {field: 'mobile', title: '手机号码', width: 120},
                {fixed: 'right', title: '操作', width: 200, align: 'center', toolbar: '#bar'}
            ]]
        });
    }, tableReload = function () {
        tObj.reload();
    }, initDialog = function(type, rowData) {
        layer.open({
            type: 1,
            title: '用户信息',
            id: 'editUserInfo',
            offset: '10px',
            area: ['430px', '490px'],
            content: '<form class="layui-form" style="width:360px;height:165px;"><div class="layui-form-item" style="margin-top: 6px"><label class="layui-form-label">用户ID:</label><div class="layui-input-block"><input id="userid" type="text" placeholder="请输入用户ID" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">用户姓名：</label><div class="layui-input-block"><input id="usernm" type="text" placeholder="请输入用户姓名" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">角色：</label><div class="layui-input-block"><select id="roleid" lay-verify="required"></select></div></div><div class="layui-form-item"><label class="layui-form-label">邮件：</label><div class="layui-input-block"><input id="email" type="text" placeholder="请输入邮件地址" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">手机：</label><div class="layui-input-block"><input id="mobile" type="text"  placeholder="请输入手机号码" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">备注：</label><div class="layui-input-block"><textarea id="remark" placeholder="请输入内容" class="layui-textarea"></textarea></div></div></form>',
            btn: ['确定', '取消'],
            yes: function(index) {
                if ($('#userid').val() == '') {
                    lAlert(layer, '您尚未输入用户标识');
                } else if ($('#usernm').val() == '') {
                    lAlert(layer, '您尚未输入用户名称!');
                } else if ($('#roleid').val() == '') {
                    lAlert(layer, '您尚未选择角色!');
                } else {
                	var url = web_path + '/auth/userinfo/adduser.do';
                	if (type == 1) {
                        url = web_path + '/auth/userinfo/moduser.do';
					}
                    layer.confirm('您是否确认提交?', {btn: ['确认', '取消'], offset: '100px'}, function () {
                        $.ajax({
                        	url: url,
                            type: 'POST',
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8',
                            data: JSON.stringify({
                                userid: $('#userid').val(),
                                usernm: $('#usernm').val(),
                                roleid: $('#roleid').val(),
                                email: $('#email').val(),
                                mobile: $('#mobile').val(),
                                remark: $('#remark').val()
                            }),
                            success: function(data){
                                lAlert(layer, data.detail);
                                if (data.result == '00') {
                                    tableReload();
                                    layer.close(index);
                                }
                            },
							error:function () {
                                if (type == 0) {
                                    lMsg(layer, '新增失败!');
                                } else if (type == 1) {
                                    lMsg(layer, '修改失败!');
                                }
                            }
                        });
                    });
                }
            },
			cancel: function (index) {
				layer.close(index);
            },
			success: function () {
                var selectedId = (type == 1 ? rowData.roleid : null);
                initCombo(function () {
                    form.render('select');
                    if (type == 1) {
                        setTableData(rowData);
                    }
                }, selectedId);
            }
        });
    }, setTableData = function(data) {
        $('#userid').val(data.userid);
        $('#usernm').val(data.usernm);
        $('#email').val(data.email);
        $('#mobile').val(data.mobile);
        $('#remark').val(data.remark);
	}, initCombo = function (callback, selectedid) {
    	if (!comboData) {
            $.post(web_path + '/auth/roleinfo/getroleinfolist.do', function (data) {
            	comboData = data;
                loadCombo($, 'roleid', comboData, selectedid);
                callback();
            });
        } else {
            loadCombo($, 'roleid', comboData, selectedid);
            callback();
        }
    };

    initTable();
    table.on('tool(user)', function(obj){
        var data = obj.data;
        if(obj.event === 'edit'){
            initDialog(1, data);
        } else if(obj.event === 'del') {
            layer.confirm('确定要删除？', function (index) {
                layer.close(index);
                $.post(web_path + '/auth/userinfo/deluser.do', {
                    userid : data.userid
                }, function(data) {
                    obj.del();
                    lMsg(layer, data.detail);
                });
            });
        }
    });
    $('#addUser').click(function(){
        initDialog(0);
    });
});