<%@page contentType="text/html;charset=UTF-8" language="java" errorPage="" %>
<%@include file="../../frame/getTime.jsp"%>
<%
	String realPath = application.getRealPath("/");
	request.setAttribute("ctx", request.getContextPath());
	request.setAttribute("t", getTime(realPath, "/js/common.js", "/view/content/auth/js/users.js"));
%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="${ctx}/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
	<legend>用户信息</legend>
	<div class="layui-btn-group">
		<button id="addUser" class="layui-btn"><i class="layui-icon">&#xe61f;</i>新增用户</button>
	</div>
	<table class="layui-table" id="datagrid" lay-filter="user"></table>

	<script type="text/html" id="bar">
		<a class="layui-btn layui-btn-mini" lay-event="edit">编辑</a>
		<a class="layui-btn layui-btn-danger layui-btn-mini" lay-event="del">删除</a>
	</script>
</div>
<script type="text/javascript" src="${ctx}/layui/layui.js"></script>
<script type="text/javascript" src="${ctx}/js/common.js?v=${t[0]}"></script>
<script type="text/javascript" src="js/users.js?v=${t[1]}"></script>
</body>
</html>