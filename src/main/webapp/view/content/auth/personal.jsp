<%@page contentType="text/html;charset=UTF-8" language="java" import="java.io.File" errorPage=""%>
<%@include file="../../frame/getTime.jsp"%>
<%
	String realPath = application.getRealPath("/");
	request.setAttribute("ctx", request.getContextPath());
	request.setAttribute("t", getTime(realPath, "/js/common.js", "/view/content/auth/js/personal.js"));
%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="${ctx}/layui/css/layui.css"/>
</head>
<body>
<div class="layui-form">
	<div>
		<button id="btn-save" class="layui-btn"><i class="layui-icon">&#xe618;</i>保存</button>
		<button id="btn-repasswd" class="layui-btn"><i class="layui-icon">&#xe642;</i>修改密码</button>
		<button id="btn-refresh" class="layui-btn"><i class="layui-icon">&#x1002;</i>刷新</button>
	</div>
	<div class="layui-form-item" style="margin-top: 20px">
		<div class="layui-inline">
			<label class="layui-form-label">用户ID</label>
			<label id="userid" class="layui-form-label" style="text-align: left"></label>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label">角色</label>
			<label id="rolenm" class="layui-form-label" style="text-align: left"></label>
		</div>
	</div>
	<div class="layui-form-item">
		<div class="layui-inline">
			<label class="layui-form-label">用户姓名</label>
			<div class="layui-input-block">
				<input type="text" id="usernm" name="usernm" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label">电子邮件</label>
			<div class="layui-input-inline">
				<input type="text" id="email" name="email" lay-verify="email" autocomplete="off" class="layui-input">
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label">手机号码</label>
			<div class="layui-input-inline">
				<input type="tel" id="mobile" name="mobile" lay-verify="phone" autocomplete="off" class="layui-input">
			</div>
		</div>
	</div>
	<div class="layui-form-item layui-form-text">
		<label class="layui-form-label">备注</label>
		<div class="layui-input-block">
			<textarea id="remark" name="remark" placeholder="备注" class="layui-textarea"></textarea>
		</div>
	</div>
</div>
<script type="text/javascript" src="${ctx}/layui/layui.js"></script>
<script type="text/javascript" src="${ctx}/js/common.js?v=${t[0]}"></script>
<script type="text/javascript" src="js/personal.js?v=${t[1]}"></script>
</body>
</html>