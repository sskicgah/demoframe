<%@page contentType="text/html;charset=UTF-8" language="java" errorPage="" %>
<%@include file="../../frame/getTime.jsp"%>
<%
	String realPath = application.getRealPath("/");
	request.setAttribute("ctx", request.getContextPath());
	request.setAttribute("t", getTime(realPath, "/js/common.js", "/view/content/auth/js/menus.js"));
%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="${ctx}/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
	<legend>菜单信息</legend>
	<div class="layui-btn-group">
		<button id="addMenu" class="layui-btn"><i class="layui-icon">&#xe61f;</i>新增菜单</button>
	</div>
	<table class="layui-table" id="datagrid" lay-filter="menu"></table>

	<script type="text/html" id="bar">
		<a class="layui-btn layui-btn-mini" lay-event="edit">编辑</a>
		<a class="layui-btn layui-btn-danger layui-btn-mini" lay-event="del">删除</a>
	</script>

	<script type="text/html" id="nodeTpl">
		{{# if(d.nodetype == '0') {
			return '父节点';
		} else {
			return '子节点';
		} }}
	</script>
</div>
<script type="text/javascript" src="${ctx}/layui/layui.js"></script>
<script type="text/javascript" src="${ctx}/js/common.js?v=${t[0]}"></script>
<script type="text/javascript" src="js/menus.js?v=${t[1]}"></script>
</body>
</html>