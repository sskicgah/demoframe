var $,element;
layui.use(['jquery', 'element'], function(){
    $ = layui.$;
    element = layui.element;
    init($);
    $.post(web_path + '/sys/frame/getmenunodes.do',
        function(json) {
            var str = '';
            for (var i = 0; i < json.length; i++) {
                str += '<li class="layui-nav-item' + (i == 0 ? ' layui-nav-itemed' : '') +'">';
                str += '<a class="" href="javascript:;">' + json[i].name + '</a>';
                if (json[i].children.length > 0) {
                    str += '<dl class="layui-nav-child">';
                }
                for (var j = 0; j < json[i].children.length; j++) {
                    str += '<dd><a onclick="addTab(\'' + json[i].children[j].id + '\', \'' + json[i].children[j].name + '\', \'' + json[i].children[j].attributes.url + '\');" href="javascript:;">' + json[i].children[j].name + '</a></dd>';
                }
                if (json[i].children.length > 0) {
                    str += '</dl>';
                }
                str += '</li>';
            }
            $('#menu-info').append(str);
            element.render();
        });
});

function addTab(id, title, url) {
    var rel_url = web_path + url;
    if (!$(".layui-tab-title>li[lay-id='" + id + "']").length) {
        var content = '<iframe scrolling="auto" frameborder="0"  src="' + rel_url + '" style="width:100%;padding:0px;margin:0px;" onload="iFrameHeight(this);"></iframe>';
        element.tabAdd('main-content', {
            title: title,
            content: content,
            id: id
        });
    }
    element.tabChange('main-content', id);
}

function iFrameHeight(obj){
    var ifm= obj;
    var subWeb = ifm.contentDocument;
    if(ifm != null && subWeb != null) {
        if (subWeb.body.scrollHeight < $(window).height() - 105) {
            ifm.height = $(window).height();
        } else {
            ifm.height = subWeb.body.scrollHeight;
        }
    }
}