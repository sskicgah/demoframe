function checkusername(username){
	//校验登录名：只能输入4-15个字母、数字、下划线
	return /^(\w){4,15}$/.test(username);
}

function checkepassword(password){
	//校验密码：只能输入6-20个字母、数字、特殊字符
	return /^(?![a-zA-z]+$)(?!\d+$)(?![!@#$%^&*.]+$)[a-zA-Z\d!@#$%^&*.]{6,20}$/.test(password)
}

function check(form1){
	if(form1.username.value == ""){
	   alert("请输入用户名!");form1.username.focus();return false;
	}
	if(!checkusername(form1.username.value)){
	   alert("您输入的用户名不合法!");form1.username.focus();return false;
	}
	if(form1.password.value == ""){
	   alert("请输入密码!");form1.password.focus();return false;
	}
	if(!checkepassword(form1.password.value)){
	   alert("您输入的密码不合法!");form1.password.focus();return false;
	}
	return true;
}

var isClick=false;
function startLogin(){
	if(!check(document.login))
		return false;
	if (!isClick) {
		document.getElementById('loginning').innerHTML = '登录中...';
        document.getElementById('loginButton').setAttribute('disabled', true);
        document.login.submit();
        isClick = true;
        document.getElementById('loginButton').removeAttribute('disabled');
        document.getElementById('loginning').innerHTML = '';
	}
}