<%@ page language="java" import="java.io.File" errorPage="" %>
<%!
public long[] getTime(String basePath, String... files) {
	int size = files.length;
	long[] t = new long[size];
	for(int i = 0; i < size; i++) {
		t[i] = (new File(basePath + files[i])).lastModified();
	}
	return t;
}
%>