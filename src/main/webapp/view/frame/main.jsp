<%@page contentType="text/html;charset=UTF-8" language="java" errorPage="" %>
<%@include file="getTime.jsp"%>
<%
	String realPath = application.getRealPath("/");
	request.setAttribute("ctx", request.getContextPath());
	request.setAttribute("t", getTime(realPath, "/js/common.js", "/view/frame/js/main.js"));
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>test平台支持系统</title>
	<link rel="stylesheet" href="${ctx}/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
	<div class="layui-header">
		<div class="layui-logo">test平台支持系统</div>
		<ul class="layui-nav layui-layout-right">
			<li class="layui-nav-item">
				<a href="javascript:;">
					${sessionScope.LOGIN_USER}
				</a>
				<dl class="layui-nav-child">
					<dd><a onclick="addTab(5, '个人信息', '/view/content/auth/personal.jsp');" href="javascript:;">个人信息</a></dd>
				</dl>
			</li>
			<li class="layui-nav-item"><a href="${ctx}/sys/logout.do">退出</a></li>
		</ul>
	</div>

	<div class="layui-side layui-bg-black">
		<div class="layui-side-scroll">
			<ul id="menu-info" class="layui-nav layui-nav-tree"  lay-filter="">
			</ul>
		</div>
	</div>

	<div class="layui-body">
		<div style="padding: 15px;">
			<div id="navTab" class="layui-tab" lay-filter="main-content" lay-allowClose="true">
				<ul class="layui-tab-title">
					<li class="layui-this">首页</li>
				</ul>
				<div class="layui-tab-content">
					<div class="layui-tab-item layui-show">
						首页
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="layui-footer">
		<!-- 底部固定区域 -->
		© test公司
	</div>
</div>
<script type="text/javascript" src="${ctx}/layui/layui.js"></script>
<script type="text/javascript" src="${ctx}/js/common.js?v=${t[0]}"></script>
<script type="text/javascript" src="js/main.js?v=${t[1]}"></script>
</body>
</html>