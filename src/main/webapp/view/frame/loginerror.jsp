<%@ page contentType="text/html; charset=UTF-8" language="java" errorPage="" %>
<% String web_path=request.getContextPath(); %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="refresh" content="5;URL=<%=web_path%>/view/frame/login.jsp">
		<link type="text/css" href="<%=web_path%>/style/frame/login_default.css" rel="stylesheet"/>
		<title>登录失败，请重新登录</title>
	</head>
	<body>
		<div>
			<div class="wrapper">
				<div class="container">
					<h2>提示信息</h2>
					<h3>对不起，您输入的用户名不存在，或密码输入错误！</h3>
					<h3>系统在5秒后自动
						<span align="right">
							<a href="<%=web_path%>/view/frame/login.jsp" />返回</a>&nbsp;！
						</span>
					</h3>
				</div>
			</div>
		</div>
	</body>
</html>