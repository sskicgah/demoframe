<%@ page contentType="text/html; charset=UTF-8" language="java" errorPage="" %>
<%@include file="getTime.jsp"%>
<% String web_path=request.getContextPath();
	String realPath = application.getRealPath("/");
	long t[] = getTime(realPath, "/view/frame/js/login.js");
%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>test平台支持系统 - 登录页面</title>
		<link rel="stylesheet" type="text/css" href="<%=web_path%>/style/frame/login_default.css">
	</head>
	<body>
		<div>
			<div class="wrapper">
				<div class="container">
					<h1>欢迎</h1>
					<form class="form" id="loginForm" name="login" method="post" action="<%=web_path%>/sys/login.do">
						<input id="username" name="username" type="text" placeholder="用户名"/>
						<input id="password" name="password" type="password" placeholder="密&nbsp;&nbsp;码"/>
						<button id="loginButton" type="button" onClick="startLogin()">登&nbsp;&nbsp;录</button>
						<div id="loginning"></div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="<%=web_path%>/view/frame/js/login.js?v=<%=t[0]%>"></script>
	</body>
</html>