CREATE TABLE `userinfo` (
  `userid` varchar(36) NOT NULL COMMENT '用户标识',
  `roleid` int(5) DEFAULT NULL COMMENT '角色标识',
  `parentid` varchar(36) DEFAULT NULL COMMENT '父标识',
  `usernm` varchar(20) DEFAULT NULL COMMENT '用户名称',
  `passwd` varchar(80) DEFAULT NULL COMMENT '用户口令',
  `email` varchar(30) DEFAULT NULL COMMENT '电子邮件',
  `mobile` varchar(15) DEFAULT NULL COMMENT '手机',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `status` int(2) DEFAULT '1' COMMENT '状态位，1为可用，0为不可用',
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
  `createtime` timestamp NOT NULL DEFAULT '2014-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';

insert into userinfo (userid, roleid, parentid, usernm, passwd, email, mobile, remark, status, createtime)
values ('admin', '0', NULL, '超级管理员', 'RjU5NkU2RUU5N0VBNTA1NzUwRkU2NTEzRDg3RUJGNjk=', '', '', '无', '1', '2014-01-01 00:00:00');/*密码123admin*/