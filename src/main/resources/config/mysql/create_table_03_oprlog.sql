CREATE TABLE `oprlog` (
  `logid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `opuserid` varchar(36) DEFAULT NULL COMMENT '操作者ID',
  `optype` varchar(20) DEFAULT NULL COMMENT '操作类型',
  `opcontent` varchar(255) DEFAULT NULL COMMENT '操作内容',
  `retcode` varchar(20) DEFAULT NULL COMMENT '响应码',
  `retmsg` varchar(255) DEFAULT NULL COMMENT '响应内容',
  `reachtime` datetime COMMENT '收到请求时间',
  `resptime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '响应时间',
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='操作日志表';