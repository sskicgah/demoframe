CREATE TABLE `menuinfo` (
  `menuid` smallint(6) NOT NULL COMMENT '菜单id',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `nodetype` varchar(10) DEFAULT NULL COMMENT '节点类型',
  `position` smallint(6) DEFAULT NULL COMMENT '位置顺序',
  `parentid` smallint(6) DEFAULT NULL COMMENT '父节点id',
  `url` varchar(255) DEFAULT NULL COMMENT '页面路径',
  `status` int(2) DEFAULT '1' COMMENT '状态位，1为可用，0为不可用',
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
  `createtime` timestamp NOT NULL DEFAULT '2014-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`menuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='功能菜单信息';

insert into menuinfo (menuid, title, nodetype, position, parentid, url, status, createtime)
values (0, '系统管理', '0', 1, -1, ' ', '1', '2014-01-01 00:00:00');

insert into MENUINFO (MENUID, TITLE, NODETYPE, POSITION, PARENTID, URL, STATUS, CREATETIME)
values (1, '角色信息设置', '1', 3, 0, '/view/content/auth/roles.jsp', '1', '2014-01-01 00:00:00');

insert into menuinfo (menuid, title, nodetype, position, parentid, url, status, createtime)
values (2, '用户信息设置', '1', 1, 0, '/view/content/auth/users.jsp', '1', '2014-01-01 00:00:00');

insert into menuinfo (menuid, title, nodetype, position, parentid, url, status, createtime)
values (3, '菜单信息设置', '1', 2, 0, '0', '/view/content/auth/menus.jsp', '1', '2014-01-01 00:00:00');

insert into menuinfo (menuid, title, nodetype, position, parentid, url, status, createtime)
values (4, '报表统计', '0', 0, -1, ' ', '1', '2014-01-01 00:00:00');

insert into menuinfo (menuid, title, nodetype, position, parentid, url, status, createtime)
values (5, '个人信息设置', '1', 1, 4, '/view/content/auth/personal.jsp', '1', '2014-01-01 00:00:00');