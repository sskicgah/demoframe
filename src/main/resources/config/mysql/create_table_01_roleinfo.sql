CREATE TABLE `roleinfo` (
  `roleid` int(11) NOT NULL COMMENT '角色标识',
  `rolenm` varchar(20) DEFAULT NULL COMMENT '角色名称',
  `lmtserial` varchar(50) NOT NULL COMMENT '权限字符串',
  `status` int(2) DEFAULT '1' COMMENT '状态位，1为可用，0为不可用',
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
  `createtime` timestamp NOT NULL DEFAULT '2014-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色信息表';

insert into roleinfo (roleid, rolenm, lmtserial, status, createtime)
values ('0', '超级管理员', '11111111111111111111111111111111111111111111111111', '1', '2014-01-01 00:00:00');