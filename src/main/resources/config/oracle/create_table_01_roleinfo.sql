create table ROLEINFO
(
  roleid    NUMBER(5) not null,
  rolenm    VARCHAR2(20),
  lmtserial VARCHAR2(50) not null,
  status    VARCHAR2(1) default '1' not null,
  updatetime DATE NOT NULL DEFAULT SYSDATE,
  createtime DATE NOT NULL DEFAULT SYSDATE,
  constraint PK_ROLEINFO primary key (roleid)
);

comment on table ROLEINFO
  is '角色信息表';

comment on column ROLEINFO.roleid
  is '角色标识';
comment on column ROLEINFO.rolenm
  is '角色名称';
comment on column ROLEINFO.lmtserial
  is '权限字符串';
comment on column ROLEINFO.status
  is '状态位，1为可用，0为不可用';
comment on column ROLEINFO.updatetime
  is '更改时间';
comment on column ROLEINFO.createtime
  is '创建时间';

insert into ROLEINFO (ROLEID, ROLENM, LMTSERIAL, STATUS, CREATETIME)
values ('0', '超级管理员', '11111111111111111111111111111111111111111111111111', '1', to_date('20140101','yyyymmdd'));