create table OPRLOG
(
  logid     INTEGER not null,
  opuserid  VARCHAR2(36),
  optype    VARCHAR2(20),
  opcontent VARCHAR2(255),
  retcode VARCHAR2(20),
  retmsg VARCHAR2(255),
  reachtime   DATE default sysdate,
  resptime    DATE default sysdate,
  constraint PK_MENU primary key (logid)
)

comment on table OPRLOG
  is '操作日志表';

comment on column OPRLOG.opuserid
  is '操作者';
comment on column OPRLOG.optype
  is '操作类型';
comment on column OPRLOG.opcontent
  is '操作内容';
comment on column OPRLOG.retcode
  is '响应码';
comment on column OPRLOG.retmsg
  is '响应内容';
comment on column OPRLOG.reachtime
  is '请求到达时间';
comment on column OPRLOG.resptime
  is '操作时间';