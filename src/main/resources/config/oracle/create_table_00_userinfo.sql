create table USERINFO
(
  userid   VARCHAR2(15) not null,
  roleid   NUMBER(5),
  parentid NUMBER(5),
  usernm   NVARCHAR2(20),
  passwd   VARCHAR2(20),
  email    VARCHAR2(30),
  mobile   VARCHAR2(15),
  remark   NVARCHAR2(50),
  status   VARCHAR2(1) default '0' not null,
  updatetime DATE NOT NULL DEFAULT SYSDATE,
  createtime DATE NOT NULL DEFAULT SYSDATE,
  constraint PK_USERINFO primary key (userid)
);

comment on table USERINFO
  is '用户信息表';

comment on column USERINFO.userid
  is '用户标识';
comment on column USERINFO.roleid
  is '角色标识';
comment on column USERINFO.parentid
  is '父标识';
comment on column USERINFO.usernm
  is '用户名称';
comment on column USERINFO.passwd
  is '用户口令';
comment on column USERINFO.email
  is '电子邮件';
comment on column USERINFO.mobile
  is '手机';
comment on column USERINFO.remark
  is '备注';
comment on column USERINFO.status
  is '账户状态';
comment on column USERINFO.updatetime
  is '更改时间';
comment on column USERINFO.createtime
  is '创建时间';

insert into USERINFO (USERID, ROLEID, PARENTID, USERNM, PASSWD, EMAIL, MOBILE, REMARK, STATUS, CREATETIME)
values ('admin', '0', NULL, '超级管理员', 'RjU5NkU2RUU5N0VBNTA1NzUwRkU2NTEzRDg3RUJGNjk=', '', '', '无', '1', to_date('20140101','yyyymmdd'));/*密码123admin*/
