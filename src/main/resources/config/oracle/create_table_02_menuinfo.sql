create table MENUINFO
(
  menuid   INTEGER not null,
  title    VARCHAR2(50),
  nodetype VARCHAR2(10),
  position INTEGER,
  parentid INTEGER,
  url      VARCHAR2(255),
  status    VARCHAR2(1) default '1' not null,
  updatetime DATE NOT NULL DEFAULT SYSDATE,
  createtime DATE NOT NULL DEFAULT SYSDATE,
  constraint PK_MENU primary key (MENUID)
);

comment on table MENUINFO
  is '功能菜单信息';

comment on column MENUINFO.menuid
  is '菜单id';
comment on column MENUINFO.title
  is '标题';
comment on column MENUINFO.nodetype
  is '节点类型';
comment on column MENUINFO.position
  is '位置顺序';
comment on column MENUINFO.parentid
  is '父节点id';
comment on column MENUINFO.url
  is '页面路径';
comment on column ROLEINFO.status
  is '状态位，1为可用，0为不可用';
comment on column ROLEINFO.updatetime
  is '更改时间';
comment on column ROLEINFO.createtime
  is '创建时间';

insert into MENUINFO (MENUID, TITLE, NODETYPE, POSITION, PARENTID, URL, STATUS, CREATETIME)
values (0, '系统管理', '0', 1, -1, ' ', '1', to_date('20140101','yyyymmdd'));

insert into MENUINFO (MENUID, TITLE, NODETYPE, POSITION, PARENTID, URL, STATUS, CREATETIME)
values (1, '角色信息设置', '1', 3, 0, '/view/content/auth/roles.jsp', '1', to_date('20140101','yyyymmdd'));

insert into MENUINFO (MENUID, TITLE, NODETYPE, POSITION, PARENTID, URL, STATUS, CREATETIME)
values (2, '用户信息设置', '1', 1, 0, '/view/content/auth/users.jsp', '1', to_date('20140101','yyyymmdd'));

insert into MENUINFO (MENUID, TITLE, NODETYPE, POSITION, PARENTID, URL, STATUS, CREATETIME)
values (3, '菜单信息设置', '1', 2, 0, '/view/content/auth/menus.jsp', '1', to_date('20140101','yyyymmdd'));

insert into MENUINFO (MENUID, TITLE, NODETYPE, POSITION, PARENTID, URL, STATUS, CREATETIME)
values (4, '报表统计', '0', 0, -1, ' ', '1', to_date('20140101','yyyymmdd'));

insert into MENUINFO (MENUID, TITLE, NODETYPE, POSITION, PARENTID, URL, STATUS, CREATETIME)
values (5, '个人信息设置', '1', 1, 4, '/view/content/auth/personal.jsp', '1', to_date('20140101','yyyymmdd'));